CREATE TABLE IF NOT EXISTS organization
(
  id   SERIAL PRIMARY KEY,
  name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS organization_user
(
  id              SERIAL PRIMARY KEY,
  username        VARCHAR(255) UNIQUE,
  password        VARCHAR(500),
  organization_id INTEGER,
   FOREIGN KEY (organization_id) REFERENCES organization (id)
);

