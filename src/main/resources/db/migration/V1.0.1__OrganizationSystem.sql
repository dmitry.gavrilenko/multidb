CREATE TABLE IF NOT EXISTS organization_system (
  id SERIAL PRIMARY KEY,
  organization_name VARCHAR(255),
  username VARCHAR(500),
  password VARCHAR(500)
);