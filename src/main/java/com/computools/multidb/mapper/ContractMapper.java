package com.computools.multidb.mapper;

import com.computools.multidb.dto.ContractDto;
import com.computools.multidb.model.Contract;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContractMapper {

    public Contract map(ContractDto contractDto) {
        Contract contract = new Contract();
        contract.name = contractDto.name;
        return contract;
    }

    public ContractDto map(Contract contract) {
        ContractDto contractDto = new ContractDto();
        contractDto.name = contract.name;
        contractDto.id = contract.id;
        return contractDto;
    }

    public List<ContractDto> map(List<Contract> contracts) {
        return contracts.stream().map(this::map).collect(Collectors.toList());
    }

}
