package com.computools.multidb.mapper;

import com.computools.multidb.dto.UserDto;
import com.computools.multidb.model.OrganizationUser;
import com.computools.multidb.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrganizationUserMapper {

    @Autowired
    private OrganizationRepository organizationRepository;

    public OrganizationUser map(UserDto userDto) {
        OrganizationUser organizationUser = new OrganizationUser();
        organizationUser.username = userDto.name;
        if (userDto.organizationId != null) {
            organizationRepository.findById(userDto.organizationId)
                    .ifPresent(o -> {
                        organizationUser.organization = o;
                    });
        }
        organizationUser.password = userDto.password;
        return organizationUser;
    }

    public UserDto map(OrganizationUser user) {
        UserDto userDto = new UserDto();
        userDto.organizationId = user.organization != null ? user.organization.id : null;
        userDto.name = user.username;
        return userDto;
    }

    public List<UserDto> map(List<OrganizationUser> organizationUsers) {
        return organizationUsers.stream().map(this::map).collect(Collectors.toList());
    }

}
