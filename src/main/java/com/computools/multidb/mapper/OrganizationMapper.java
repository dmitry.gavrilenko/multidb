package com.computools.multidb.mapper;

import com.computools.multidb.dto.OrganizationDto;
import com.computools.multidb.model.Organization;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrganizationMapper {

    public Organization map(OrganizationDto organizationDto) {
        Organization organization = new Organization();
        organization.name = organizationDto.name;
        return organization;
    }

    public OrganizationDto map(Organization organization) {
        OrganizationDto organizationDto = new OrganizationDto();
        organizationDto.id = organization.id;
        organizationDto.name = organization.name;
        return organizationDto;
    }

    public List<OrganizationDto> map(List<Organization> organizations) {
        return organizations.stream().map(this::map).collect(Collectors.toList());
    }

}
