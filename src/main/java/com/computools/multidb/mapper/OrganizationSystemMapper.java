package com.computools.multidb.mapper;

import com.computools.multidb.model.OrganizationSystem;
import com.computools.multidb.model.OrganizationUser;
import org.springframework.stereotype.Component;

@Component
public class OrganizationSystemMapper {

    public OrganizationSystem map(OrganizationUser user) {
        OrganizationSystem system = new OrganizationSystem();
        system.password = user.password;
        system.username = user.username;
        system.organizationName = user.organization.name;
        return system;
    }

}
