package com.computools.multidb.factory;

import com.computools.multidb.model.Contract;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Properties;

public class Factory {

    private Factory() {}

    public static SessionFactory sessionFactory(String url, String username, String password) {
        return config(url, username, password).buildSessionFactory();
    }

    private static Properties props(String url, String username, String password) {
        Properties props = new Properties();
        props.put("hibernate.connection.driver_class", "org.postgresql.Driver");
        props.put("hibernate.connection.url", url);
        props.put("hibernate.connection.username", username);
        props.put("hibernate.connection.password", password);
        return props;
    }

    private static Configuration config(String url, String username, String password) {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Contract.class);
        configuration.setProperties(props(url, username, password));
        return configuration;
    }

}
