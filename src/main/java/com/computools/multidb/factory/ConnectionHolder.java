package com.computools.multidb.factory;

import com.computools.multidb.model.OrganizationSystem;
import org.hibernate.SessionFactory;

import java.util.HashMap;
import java.util.Map;

public class ConnectionHolder {

    private ConnectionHolder () {}

    public static Map<Long, SessionFactory> connections = new HashMap<>();

    public static void add(Long userId, OrganizationSystem system, String host) { //add if not exists
        if (ConnectionHolder.connections.get(userId) == null) {
            ConnectionHolder.connections.put(
                    userId, Factory.sessionFactory(
                            String.format("jdbc:postgresql://%s:5432/%s", host, system.organizationName),
                            system.username, system.password
                    )
            );
        }
    }

    public static void remove(Long userId) {
        connections.remove(userId);
    }

}
