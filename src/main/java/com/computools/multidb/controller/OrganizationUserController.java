package com.computools.multidb.controller;

import com.computools.multidb.dto.UserDto;
import com.computools.multidb.mapper.OrganizationUserMapper;
import com.computools.multidb.service.OrganizationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class OrganizationUserController {

    @Autowired
    private OrganizationUserService organizationUserService;

    @Autowired
    private OrganizationUserMapper organizationUserMapper;

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable Long id) throws IllegalAccessException {
        return organizationUserMapper.map(organizationUserService.getOrganizationUser(id));
    }

    @GetMapping
    public List<UserDto> getUsers() {
        return organizationUserMapper.map(organizationUserService.getOrganizationUsers());
    }

    @PostMapping("/sign-up")
    public UserDto save(@RequestBody UserDto userDto) {
        return organizationUserMapper.map(organizationUserService.save(organizationUserMapper.map(userDto)));
    }

}
