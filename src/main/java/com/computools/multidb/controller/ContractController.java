package com.computools.multidb.controller;

import com.computools.multidb.dto.ContractDto;
import com.computools.multidb.mapper.ContractMapper;
import com.computools.multidb.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.List;

@RestController
@RequestMapping("/contracts")
public class ContractController {

    @Autowired
    private ContractService contractService;

    @Autowired
    private ContractMapper contractMapper;

    @GetMapping("/{id}")
    public ContractDto getContract(@PathVariable Long id) throws AuthenticationException {
        return contractMapper.map(contractService.getContact(id));
    }

    @GetMapping
    public List<ContractDto> getContracts() throws AuthenticationException {
        return contractMapper.map(contractService.getContracts());
    }

    @PostMapping
    public ContractDto save(@RequestBody ContractDto contractDto) throws AuthenticationException {
        return contractMapper.map(contractService.save(contractMapper.map(contractDto)));
    }

}
