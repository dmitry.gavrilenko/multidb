package com.computools.multidb.controller;

import com.computools.multidb.dto.OrganizationDto;
import com.computools.multidb.mapper.OrganizationMapper;
import com.computools.multidb.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organizations")
public class OrganizationController {

    @Autowired
    private OrganizationMapper organizationMapper;

    @Autowired
    private OrganizationService organizationService;

    @GetMapping("/{id}")
    public OrganizationDto findById(@PathVariable Long id) {
        return organizationMapper.map(organizationService.getOrganization(id));
    }

    @GetMapping
    public List<OrganizationDto> findAll() {
        return organizationMapper.map(organizationService.getOrganizations());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrganizationDto create(@RequestBody OrganizationDto organizationDto) throws InterruptedException {
        return organizationMapper.map(organizationService.save(organizationMapper.map(organizationDto)));
    }

}
