package com.computools.multidb.dto;

import org.springframework.lang.NonNull;

public class UserDto {

    public String name;

    @NonNull
    public Long organizationId;

    public String password;

}
