package com.computools.multidb.dto;

import com.computools.multidb.exception.GlobalException;
import org.springframework.http.HttpStatus;

public class ApiError {

    private HttpStatus status;

    private String message;

    private long timestamp;

    public ApiError(GlobalException ex) {
        this.status = ex.status;
        this.message = ex.message;
        this.timestamp = ex.timestamp;
    }

    public HttpStatus status() {
        return status;
    }

    public String message() {
        return message;
    }

    public long timestamp() {
        return timestamp;
    }
}
