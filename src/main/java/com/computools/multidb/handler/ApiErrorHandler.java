package com.computools.multidb.handler;

import com.computools.multidb.dto.ApiError;
import com.computools.multidb.exception.GlobalException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApiErrorHandler {

    @ExceptionHandler(GlobalException.class)
    public ApiError apiError(GlobalException e) {
        return new ApiError(e);
    }

}
