package com.computools.multidb.model;

import javax.persistence.*;

@Entity
@Table(name = "organization_user")
public class OrganizationUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(unique = true)
    public String username;

    public String password;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "organization_id", referencedColumnName = "id")
    public Organization organization;
}
