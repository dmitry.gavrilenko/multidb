package com.computools.multidb.model;

import javax.persistence.*;

@Entity
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "organization")
    public OrganizationUser organizationUser;

}
