package com.computools.multidb.service;

import com.computools.multidb.model.Organization;

import java.util.List;

public interface OrganizationService {

    Organization getOrganization(Long id);

    List<Organization> getOrganizations();

    Organization save(Organization organization) throws InterruptedException;

}
