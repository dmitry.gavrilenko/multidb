package com.computools.multidb.service;

import com.computools.multidb.management.DatabaseManagement;
import com.computools.multidb.model.Organization;
import com.computools.multidb.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private DatabaseManagement databaseManagement;

    @Override
    public Organization getOrganization(Long id) {
        return organizationRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<Organization> getOrganizations() {
        return organizationRepository.findAll();
    }

    @Override
    public Organization save (Organization organization) throws InterruptedException {
        databaseManagement.createDatabase(organization.name);
        return organizationRepository.save(organization);
    }

}
