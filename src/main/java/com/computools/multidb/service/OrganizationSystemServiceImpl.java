package com.computools.multidb.service;

import com.computools.multidb.exception.AuthenticationException;
import com.computools.multidb.management.AuthorityManagement;
import com.computools.multidb.model.OrganizationSystem;
import com.computools.multidb.model.OrganizationUser;
import com.computools.multidb.repository.OrganizationSystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrganizationSystemServiceImpl implements OrganizationSystemService {

    @Autowired
    private OrganizationSystemRepository organizationSystemRepository;

    @Autowired
    private AuthorityManagement authorityManagement;

    @Override
    public Optional<OrganizationSystem> getOrganizationSystem() {
        OrganizationUser user = authorityManagement.getUser().orElseThrow(AuthenticationException::new);
        return organizationSystemRepository.findByUsername(user.username);
    }
}
