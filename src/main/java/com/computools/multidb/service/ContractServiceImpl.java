package com.computools.multidb.service;

import com.computools.multidb.model.Contract;
import com.computools.multidb.repository.ContractRepository;
import com.computools.multidb.repository.SessionContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private SessionContractRepository contractRepository;

    @Override
    public Contract getContact(Long id) {
        return contractRepository.findById(id);
    }

    @Override
    public List<Contract> getContracts() {
        return contractRepository.findAll();
    }

    @Override
    public Contract save(Contract contract) {
        return contractRepository.save(contract);
    }
}
