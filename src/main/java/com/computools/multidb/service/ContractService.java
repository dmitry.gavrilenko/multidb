package com.computools.multidb.service;

import com.computools.multidb.model.Contract;

import javax.naming.AuthenticationException;
import java.util.List;

public interface ContractService {

    Contract getContact(Long id) throws AuthenticationException;

    List<Contract> getContracts() throws AuthenticationException;

    Contract save(Contract contract) throws AuthenticationException;
}
