package com.computools.multidb.service;

import com.computools.multidb.model.OrganizationSystem;

import java.util.Optional;

public interface OrganizationSystemService {

    Optional<OrganizationSystem> getOrganizationSystem();

}
