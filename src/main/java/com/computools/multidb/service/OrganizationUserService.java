package com.computools.multidb.service;

import com.computools.multidb.model.OrganizationUser;

import java.util.List;

public interface OrganizationUserService {

    OrganizationUser getOrganizationUser(Long id) throws IllegalAccessException;

    List<OrganizationUser> getOrganizationUsers();

    OrganizationUser save(OrganizationUser organizationUser);

}
