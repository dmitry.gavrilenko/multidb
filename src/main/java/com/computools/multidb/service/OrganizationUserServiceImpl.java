package com.computools.multidb.service;

import com.computools.multidb.management.DatabaseManagement;
import com.computools.multidb.mapper.OrganizationSystemMapper;
import com.computools.multidb.model.OrganizationUser;
import com.computools.multidb.repository.OrganizationSystemRepository;
import com.computools.multidb.repository.OrganizationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class OrganizationUserServiceImpl implements OrganizationUserService {

    @Autowired
    private OrganizationUserRepository organizationUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private OrganizationSystemMapper organizationSystemMapper;

    @Autowired
    private OrganizationSystemRepository organizationSystemRepository;

    @Autowired
    private DatabaseManagement databaseManagement;

    @Override
    public OrganizationUser getOrganizationUser(Long id) throws IllegalAccessException {
        return organizationUserRepository.findById(id).orElseThrow(IllegalAccessException::new);
    }

    @Override
    public List<OrganizationUser> getOrganizationUsers() {
        return organizationUserRepository.findAll();
    }

    @Override
    @Transactional
    public OrganizationUser save(OrganizationUser organizationUser) {
        organizationSystemRepository.save(organizationSystemMapper.map(organizationUser));
        databaseManagement.createUser(organizationUser.organization.name, organizationUser.username, organizationUser.password);
        organizationUser.password = passwordEncoder.encode(organizationUser.password);
        organizationUserRepository.save(organizationUser);
        return organizationUser;
    }
}
