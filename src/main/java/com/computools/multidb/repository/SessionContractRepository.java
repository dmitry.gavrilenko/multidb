package com.computools.multidb.repository;

import com.computools.multidb.exception.OrganizationSystemException;
import com.computools.multidb.factory.ConnectionHolder;
import com.computools.multidb.factory.Factory;
import com.computools.multidb.management.AuthorityManagement;
import com.computools.multidb.model.Contract;
import com.computools.multidb.model.OrganizationSystem;
import com.computools.multidb.service.OrganizationSystemService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SessionContractRepository {

    @Autowired
    private OrganizationSystemService organizationSystemService;

    @Autowired
    private AuthorityManagement authorityManagement;


    @Value("${docker.db.host}")
    private String host;

    public Contract findById(Long id) {
        OrganizationSystem system = organizationSystemService.getOrganizationSystem().orElseThrow(IllegalArgumentException::new);
        ConnectionHolder.add(authorityManagement.getUser().orElseThrow(IllegalArgumentException::new).id, system, host);
        Contract contract;
        SessionFactory sessionFactory = ConnectionHolder.connections.get(authorityManagement.getUser().orElseThrow(IllegalArgumentException::new).id);
        try (Session session = sessionFactory.openSession()) {
            contract = session.get(Contract.class, id);
        }
        return contract;
    }

    public List<Contract> findAll() {
        OrganizationSystem system = organizationSystemService.getOrganizationSystem().orElseThrow(IllegalArgumentException::new);
        ConnectionHolder.add(authorityManagement.getUser().orElseThrow(IllegalArgumentException::new).id, system, host);
        List<Contract> contracts;
        SessionFactory sessionFactory = ConnectionHolder.connections.get(authorityManagement.getUser().orElseThrow(IllegalArgumentException::new).id);
        try (Session session = sessionFactory.openSession()) {
            contracts = session.createQuery("FROM Contract").list();
        }
        return contracts;
    }

    public Contract save(Contract contract) {
        OrganizationSystem system = organizationSystemService.getOrganizationSystem().orElseThrow(IllegalArgumentException::new);
        ConnectionHolder.add(authorityManagement.getUser().orElseThrow(IllegalArgumentException::new).id, system, host);
        Transaction transaction;
        SessionFactory sessionFactory = ConnectionHolder.connections.get(authorityManagement.getUser().orElseThrow(IllegalArgumentException::new).id);
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(contract);
            transaction.commit();
        }
        return contract;
    }

}
