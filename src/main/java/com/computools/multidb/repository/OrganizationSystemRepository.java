package com.computools.multidb.repository;

import com.computools.multidb.model.OrganizationSystem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrganizationSystemRepository extends JpaRepository<OrganizationSystem, Long> {

    Optional<OrganizationSystem> findByUsername(String username);

}
