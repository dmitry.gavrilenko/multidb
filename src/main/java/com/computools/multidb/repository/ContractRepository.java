package com.computools.multidb.repository;

import com.computools.multidb.model.Contract;

import java.util.List;
import java.util.Optional;

public interface ContractRepository {

    Contract findById(Long id);

    List<Contract> findAll();

    Contract save(Contract contract);

}
