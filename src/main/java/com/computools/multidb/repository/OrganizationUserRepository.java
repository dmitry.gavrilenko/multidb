package com.computools.multidb.repository;

import com.computools.multidb.model.OrganizationUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrganizationUserRepository extends JpaRepository<OrganizationUser, Long> {

    Optional<OrganizationUser> findByUsername(String username);

}
