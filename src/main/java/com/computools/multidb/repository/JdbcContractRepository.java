package com.computools.multidb.repository;

import com.computools.multidb.exception.OrganizationSystemException;
import com.computools.multidb.management.DatasourceManagement;
import com.computools.multidb.model.Contract;
import com.computools.multidb.model.OrganizationSystem;
import com.computools.multidb.service.OrganizationSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class JdbcContractRepository implements ContractRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DatasourceManagement datasourceManagement;

    @Autowired
    private OrganizationSystemService organizationSystemService;

    @Override
    public Contract findById(Long id) {
        OrganizationSystem system = organizationSystemService.getOrganizationSystem().orElseThrow(OrganizationSystemException::new);
        jdbcTemplate.setDataSource(datasourceManagement.buildDatasource(system.organizationName, system.username, system.password));
        List<Contract> contracts = jdbcTemplate.query(String.format("SELECT * FROM contract WHERE id = %s", id), (rs, rowNum) -> {
            Contract contract = new Contract();
            contract.id = rs.getLong(1);
            contract.name = rs.getString(2);
            return contract;
        });
        return contracts.get(0);
    }

    @Override
    public List<Contract> findAll() {
        OrganizationSystem system = organizationSystemService.getOrganizationSystem().orElseThrow(OrganizationSystemException::new);
        jdbcTemplate.setDataSource(datasourceManagement.buildDatasource(system.organizationName, system.username, system.password));
        return jdbcTemplate.query("SELECT * FROM contract", (rs, rowNum) -> {
            Contract contract = new Contract();
            contract.id = rs.getLong(1);
            contract.name = rs.getString(2);
            return contract;
        });
    }

    @Override
    public Contract save(Contract contract) {
        OrganizationSystem system = organizationSystemService.getOrganizationSystem().orElseThrow(OrganizationSystemException::new);
        jdbcTemplate.setDataSource(datasourceManagement.buildDatasource(system.organizationName, system.username, system.password));
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO contract(name) VALUES(?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, contract.name);
            return ps;
        }, holder);
        contract.id = ((Integer)holder.getKeys().get("id")).longValue();
        return contract;
    }

}
