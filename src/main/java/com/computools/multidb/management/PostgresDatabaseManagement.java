package com.computools.multidb.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class PostgresDatabaseManagement implements DatabaseManagement {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DatasourceManagement datasourceManagement;

    @Override
    public void createDatabase(String name) {
        jdbcTemplate.setDataSource(datasourceManagement.buildDatasource("system"));
        jdbcTemplate.execute(String.format("CREATE DATABASE %s", name));
        jdbcTemplate.setDataSource(datasourceManagement.buildDatasource(name));
        jdbcTemplate.execute("CREATE TABLE contract(id SERIAL PRIMARY KEY, name VARCHAR(255))");
    }

    @Override
    public void createUser(String databaseName, String username, String password) {
        jdbcTemplate.setDataSource(datasourceManagement.buildDatasource(databaseName));
        jdbcTemplate.execute(String.format("CREATE USER %s WITH password '%s'", username, password));
        jdbcTemplate.execute(String.format("ALTER DATABASE %s OWNER TO %s", databaseName, username));
        jdbcTemplate.execute(String.format("ALTER TABLE contract OWNER TO %s", username));
    }

}
