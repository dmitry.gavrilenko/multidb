package com.computools.multidb.management;

public interface DatabaseManagement {

    void createDatabase(String name) throws InterruptedException;

    void createUser(String databaseName, String username, String password);

}
