package com.computools.multidb.management;

import com.computools.multidb.model.OrganizationUser;

import java.util.Optional;

public interface AuthorityManagement {

    Optional<OrganizationUser> getUser();

}
