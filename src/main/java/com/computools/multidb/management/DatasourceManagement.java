package com.computools.multidb.management;

import javax.sql.DataSource;

public interface DatasourceManagement {

    DataSource buildDatasource(String databaseName);

    DataSource buildDatasource(String databaseName, String username, String password);

}
