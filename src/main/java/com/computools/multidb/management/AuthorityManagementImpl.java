package com.computools.multidb.management;

import com.computools.multidb.model.OrganizationUser;
import com.computools.multidb.repository.OrganizationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthorityManagementImpl implements AuthorityManagement {

    @Autowired
    private OrganizationUserRepository organizationUserRepository;

    @Override
    public Optional<OrganizationUser> getUser() {
        UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return organizationUserRepository.findByUsername(details.getUsername());
    }

}
