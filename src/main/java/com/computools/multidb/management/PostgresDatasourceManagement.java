package com.computools.multidb.management;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class PostgresDatasourceManagement implements DatasourceManagement {

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassname;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${docker.db.host}")
    private String host;

    @Override
    public DataSource buildDatasource(String databaseName) {
        return DataSourceBuilder.create()
                .driverClassName(driverClassname)
                .url(String.format("jdbc:postgresql://%s:5432/%s",host, databaseName))
                .username(username)
                .password(password)
                .build();
    }

    @Override
    public DataSource buildDatasource(String databaseName, String username, String password) {
        return DataSourceBuilder.create()
                .driverClassName(driverClassname)
                .url(String.format("jdbc:postgresql://%s:5432/%s",host, databaseName))
                .username(username)
                .password(password)
                .build();
    }

}
