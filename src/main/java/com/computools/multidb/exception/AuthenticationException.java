package com.computools.multidb.exception;

import org.springframework.http.HttpStatus;

public class AuthenticationException extends GlobalException {

    public AuthenticationException() {
        super.message = "Authentication field, please try again";
        super.status = HttpStatus.UNAUTHORIZED;
    }

    public AuthenticationException(HttpStatus status) {
        super(status);
        super.message = "Authentication field, please try again";
    }

    public AuthenticationException(String message) {
        super(message);
    }
}
