package com.computools.multidb.exception;

import org.springframework.http.HttpStatus;

public class OrganizationSystemException extends GlobalException {

    public OrganizationSystemException() {
        super.message = "OrganizationSystem not found";
        super.status = HttpStatus.NOT_FOUND;
    }

    public OrganizationSystemException(HttpStatus status) {
        super(status);
    }

    public OrganizationSystemException(String message) {
        super(message);
    }

    public OrganizationSystemException(HttpStatus status, String message) {
        super(status, message);
    }
}
