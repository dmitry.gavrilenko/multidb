package com.computools.multidb.exception;

import org.springframework.http.HttpStatus;

public class GlobalException extends RuntimeException {

    public HttpStatus status;

    public String message;

    public long timestamp;

    public GlobalException() {
    }

    public GlobalException(HttpStatus status) {
        this.status = status;
        timestamp = System.currentTimeMillis();
    }

    public GlobalException(String message) {
        this.message = message;
        timestamp = System.currentTimeMillis();
    }

    public GlobalException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
        timestamp = System.currentTimeMillis();
    }

}
