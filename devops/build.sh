#!/usr/bin/env bash
cd ../
mvn clean package -DskipTests
cd devops
docker-compose up --build
